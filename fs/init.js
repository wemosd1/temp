// Load Mongoose OS API
load('api_timer.js');
load('api_http.js');
load('api_arduino_onewire.js');
load('api_arduino_dallas_temp.js');
load('api_config.js');

// GPIO pin which has sensors data wire connected
let pin = 4;

// Initialize 1-Wire bus
let ow = OneWire.create(pin);
// Initialize DallasTemperature library
let dt = DallasTemperature.create(ow);
// Start up the library
dt.begin();
// Number of sensors found on the 1-Wire bus
let n = 0;
// Sensors addresses
let sens = [];
let deviceID = Cfg.get("ilmajaam.device_id");

// This function reads data from the DS sensors every 2 seconds
Timer.set(Cfg.get("ilmajaam.update_interval") /* milliseconds */, true /* repeat */, function() {
  if (n === 0) {
    sensorsCount = dt.getDeviceCount();
    print('Sensors found:', sensorsCount);

    for (let i = 0; i < sensorsCount; i++) {
      sens[i] = '01234567';
      if (dt.getAddress(sens[i], i) === 1) {
        print('Sensor#', i, 'address:', dt.toHexStr(sens[i]));
      }
    }
  }
  dt.requestTemperatures();

  let str = JSON.stringify(dt.getTempC(sens[0]));
  
  if (deviceID !== '' && sensorsCount > 0) {
    print("Sending data");
    let url = 'http://temperature.sipsu.eu/temperature.php?place=' + deviceID;

    for (let i = 0; i < sensorsCount; i++) {
      let str = JSON.stringify(dt.getTempC(sens[i]));
      url += '&temp[]=' + str
      print('Sensor #', i, 'Temperature:', str, '*C');
    }
  
    HTTP.query({
    url: url,
      success: function(body, full_http_msg) { 
      },
      error: function(err) {
        
      },
    });    
  }
  else {
    print("No temperature sensors detected!");
  }
}, null);