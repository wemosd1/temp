@echo off

set /p port=COM Port like (12)?: 
set /p SSID=WIFI SSID: 
set /p pwd=WIFI Password: 
set /p device=WIFI Device ID: 

ren mos build
mos flash --port COM%port% --platform esp8266
mos --port COM%port% config-set wifi.sta.ssid=%SSID% wifi.sta.pass=%pwd% mqtt.server=zabbix.sipsu.eu:1883 mqtt.enable=true ilmajaam.device_id=%device% wifi.sta.enable=true
mos --port COM%port% console