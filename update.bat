@echo off

set /p port=COM Port like (12)?: 

mos --port COM%port% get conf9.json > conf9.json
mos flash --port COM%port% --platform esp8266
mos --port COM%port% put conf9.json conf9.json
del conf9.json
mos --port COM%port% call Sys.Reboot > nul
sleep 3
mos --port COM%port% config-set mqtt.server=zabbix.sipsu.eu:1883 mqtt.enable=true
sleep 3
mos --port COM%port% call Sys.Reboot > nul
mos --port COM%port% console